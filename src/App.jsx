import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

import CreateService from "./components/views/CreateService";
import ServicesList from "./components/views/ServicesList";
import Detail from "./components/views/Detail";
import Service from "./components/views/Service";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID,
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const hd = getStorage(app);

function App() {
  return (
    <BrowserRouter>
      <Route exact path="/">
        <ServicesList database={db} />
      </Route>
      <Route exact path="/create">
        <CreateService database={db} />
      </Route>
      <Route exact path="/detail/:id">
        <Detail database={db} />
      </Route>
      <Route exact path="/service/:id">
        <Service database={db} storage={hd} />
      </Route>
    </BrowserRouter>
  );
}

export default App;
