import React from "react";

export const Spacer = ({ height = "20px" }) => {
  return <div style={{ height }} />;
};
