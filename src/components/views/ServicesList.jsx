import React from "react";
import Chip from "@mui/material/Chip";
import {
  collection,
  query,
  orderBy,
  startAfter,
  limit,
  getDocs,
  where,
} from "firebase/firestore";

import Container from "./../dumb/Container";
import TableServices from "./../smart/TableServices";
import { findStatus } from "./../../utils/constants";

const ServicesList = ({ database }) => {
  const [servicesList, setSetvicesList] = React.useState([]);
  const [lastServiceVisible, setLastServiceVisible] = React.useState(null);

  const getServices = async (db) => {
    const servicesCol = query(
      collection(db, "services"),
      orderBy("nt", "desc"),
      limit(7)
    );
    const serviceSnapshot = await getDocs(servicesCol);
    const serviceList = serviceSnapshot.docs.map((doc) => doc.data());

    const lastVisible = serviceSnapshot.docs[serviceSnapshot.docs.length - 1];
    setLastServiceVisible(lastVisible);

    return serviceList;
  };

  const getMoreServices = async () => {
    const nextServices = query(
      collection(database, "services"),
      orderBy("nt", "desc"),
      startAfter(lastServiceVisible),
      limit(7)
    );
    const serviceSnapshot = await getDocs(nextServices);
    const serviceList = serviceSnapshot.docs.map((doc) => doc.data());

    const lastVisible = serviceSnapshot.docs[serviceSnapshot.docs.length - 1];
    setLastServiceVisible(lastVisible);

    normalizeServices(serviceList, true);
  };

  const getSearchService = async (searchQuery) => {
    const servicesRef = collection(database, "services");

    const q = query(servicesRef, where("nt", "==", searchQuery));

    const serviceSnapshot = await getDocs(q);
    const serviceList = serviceSnapshot.docs.map((doc) => doc.data());

    if (serviceList.length === 0)
      return alert("no se encontro ningun servicio");

    normalizeServices(serviceList);
  };

  const getSearchServiceDni = async (searchQuery) => {
    const servicesRef = collection(database, "services");

    const q = query(servicesRef, where("dni", "==", searchQuery));

    const serviceSnapshot = await getDocs(q);
    const serviceList = serviceSnapshot.docs.map((doc) => doc.data());

    if (serviceList.length === 0)
      return alert("no se encontro ningun servicio");

    normalizeServices(serviceList);
  };

  const getSearchServiceStatus = async (statusQuery) => {
    const servicesRef = collection(database, "services");

    const q = query(servicesRef, where("status", "==", statusQuery));

    const serviceSnapshot = await getDocs(q);
    const serviceList = serviceSnapshot.docs.map((doc) => doc.data());

    if (serviceList.length === 0)
      return alert("no se encontro ningun servicio");

    normalizeServices(serviceList);
  };

  const createData = (name, client, article, status, date, service) => {
    const articleStatus = findStatus(status);

    return {
      name,
      client,
      article,
      status: <Chip label={articleStatus.name} color={articleStatus.color} />,
      date,
      service,
    };
  };

  const normalizeServices = (vanillaServices, pagination = false) => {
    const formattedServices = vanillaServices.map((service) => {
      const createdFormatted = new Date(parseInt(service.createdAt));

      const createdAt = new Intl.DateTimeFormat("es-AR", {
        dateStyle: "short",
        timeStyle: "short",
      }).format(createdFormatted);

      const articles = `${service.articles.length} (${
        service.articles[0].item
      }${service.articles.length > 1 ? ", ..." : ""})`;

      return createData(
        service.nt,
        service.name,
        articles,
        service.status,
        createdAt,
        service
      );
    });

    const list = pagination
      ? [...servicesList, ...formattedServices]
      : formattedServices;

    setSetvicesList(list);
  };

  React.useEffect(() => {
    getServices(database).then((dbServices) => {
      normalizeServices(dbServices);
    });
  }, []);

  return (
    <Container viewName="Lista de Articulos">
      <TableServices
        servicesList={servicesList}
        getMoreServices={getMoreServices}
        getSearchService={getSearchService}
        getSearchServiceDni={getSearchServiceDni}
        getSearchServiceStatus={getSearchServiceStatus}
      />
    </Container>
  );
};

export default ServicesList;
