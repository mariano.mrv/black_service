import * as React from "react";
import { useParams } from "react-router";
import { collection, query, where, getDocs } from "firebase/firestore";

import { DetailView } from "../smart/DetailView";
import Container from "./../dumb/Container";
import { ServiceBody, ServiceArticlesBody } from "../../utils/models";

export default function Detail({ database }) {
  const { id } = useParams();
  const [service, setService] = React.useState({
    ...ServiceBody,
    articles: [ServiceArticlesBody],
  });
  const [movements, setMovements] = React.useState([]);

  const getItem = async () => {
    const q = query(
      collection(database, "services"),
      where("nt", "==", parseFloat(id))
    );
    const querySnapshot = await getDocs(q);

    querySnapshot.forEach((doc) => {
      setService(doc.data());
    });
  };

  const getHistory = async (db) => {
    const movementsCol = query(
      collection(db, "movements"),
      where("nt", "==", parseFloat(id))
    );

    const movementSnapshot = await getDocs(movementsCol);
    const movementList = movementSnapshot.docs.map((doc) => doc.data());

    setMovements(movementList);
  };

  React.useEffect(() => {
    getItem();
    getHistory(database);
  }, []);

  return (
    <Container viewName={`Detalle de producto (${service.nt || "..."})`}>
      <DetailView service={service} movementList={movements} />
    </Container>
  );
}
