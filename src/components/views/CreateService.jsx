import React from "react";
import { useHistory } from "react-router";
import {
  collection,
  query,
  setDoc,
  doc,
  getDocs,
  where,
} from "firebase/firestore";

import Container from "../dumb/Container";
import CreateServiceForm from "../smart/CreateServiceForm";
import { Urls } from "../../utils/constants";

const CreateService = ({ database }) => {
  const history = useHistory();

  const [loading, setLoading] = React.useState(false);

  const handleCreateService = async (serviceItem) => {
    setLoading(true);
    const id = new Date().valueOf().toString();
    let tech = "";

    const servicesCol = query(collection(database, "services"));
    const serviceSnapshot = await getDocs(servicesCol);

    const lastNtNumber = serviceSnapshot.docs.map((doc) => {
      const item = doc.data();

      return item.nt;
    });

    const serviceCuantity = Math.max(...lastNtNumber) + 1;

    // verify receptor
    const checkReceptor = query(
      collection(database, "users"),
      where("codigo", "==", serviceItem.receptor)
    );

    const receptorSnapshot = await getDocs(checkReceptor);
    const receptorFound = receptorSnapshot.docs.length.toString();

    if (receptorFound !== "0") {
      receptorSnapshot.forEach((doc) => {
        const receptor = doc.data();
        tech = receptor.nombre;
      });

      // confirm create service
      await setDoc(doc(database, "services", id), {
        ...serviceItem,
        nt: parseFloat(serviceCuantity),
        createdAt: id,
        receptor: tech,
      })
        .then(async () => {
          await setDoc(doc(database, "movements", id), {
            ...serviceItem,
            nt: parseFloat(serviceCuantity),
            createdAt: id,
            receptor: tech,
            fixed_by: tech,
          });

          history.push(Urls.homeUrl());
        })
        .catch(() => {
          alert("hubo un error al crear el servicio");
        });
    } else {
      alert("tecnico no encontrado");
    }

    setLoading(false);
  };

  return (
    <Container viewName="Ingresar Articulo">
      <CreateServiceForm
        isLoading={loading}
        onCreateService={handleCreateService}
      />
    </Container>
  );
};

export default CreateService;
