import * as React from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router";
import {
  collection,
  query,
  where,
  getDocs,
  setDoc,
  doc,
} from "firebase/firestore";

import { ServiceForm } from "../smart/ServiceForm";
import Container from "./../dumb/Container";
import { ServiceBody, ServiceArticlesBody } from "../../utils/models";

export default function Service({ database, storage }) {
  const { id } = useParams();
  const history = useHistory();

  const [service, setService] = React.useState({
    ...ServiceBody,
    articles: [ServiceArticlesBody],
  });

  const getService = async () => {
    const q = query(
      collection(database, "services"),
      where("nt", "==", parseFloat(id))
    );
    const querySnapshot = await getDocs(q);

    querySnapshot.forEach((doc) => {
      setService(doc.data());
    });
  };

  const handleChangeStatus = (event) => {
    setService({
      ...service,
      status: event.target.value,
    });
  };

  const handleUpdateService = (name, value) => {
    setService({ ...service, [name]: value });
  };

  const handleConfirmService = async () => {
    await setDoc(doc(database, "services", service.createdAt), service)
      .then(async () => {
        const created_at = new Date().valueOf().toString();

        await setDoc(doc(database, "movements", created_at), {
          ...service,
          createdAt: created_at,
        }).then(() => {
          alert("se actualizo correctamente");
          history.push("/");
        });
      })
      .catch(() => {
        alert("hubo un error al crear el servicio");
      });
  };

  React.useEffect(() => {
    getService();
  }, []);

  return (
    <Container viewName={`Detalle de producto (${service.nt || "..."})`}>
      <ServiceForm
        service={service}
        storage={storage}
        handleChangeStatus={handleChangeStatus}
        handleUpdateService={handleUpdateService}
        handleConfirmService={handleConfirmService}
      />
    </Container>
  );
}
