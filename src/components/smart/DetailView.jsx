import React from "react";
import { useHistory } from "react-router";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Stack from "@mui/material/Stack";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Chip from "@mui/material/Chip";
import Modal from "@mui/material/Modal";

import { Spacer } from "./../dumb/utils";
import {
  boxCustomStyle,
  formatDate,
  formatIntlDate,
  notificationMessages,
  paperCustomStyle,
  Urls,
  findStatus,
} from "../../utils/constants";
import { FormControl } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
export const DetailView = ({ service, movementList }) => {
  const history = useHistory();

  const [open, setOpen] = React.useState(false);

  const handleClose = () => setOpen(false);

  const [message, setMessage] = React.useState(notificationMessages[1]);

  React.useEffect(() => {
    switch (service.status) {
      case "1":
        setMessage(notificationMessages[1]);
        break;
      case "2":
        setMessage(notificationMessages[2]);
        break;
      case "3":
        setMessage(notificationMessages[3]);
        break;
      default:
        setMessage(notificationMessages[1]);
        break;
    }
  }, [service.nt]);

  const onNotify = (tel, numero) => {
    let mensaje = "Su equipo N°";
    let msg = "";
    let link = "https://api.whatsapp.com/send?phone=";

    for (let i = 1; i <= mensaje.length; i++) {
      mensaje = mensaje.replace(" ", "%20");
    }
    for (let j = 1; j <= message.length; j++) {
      msg = message.replace(" ", "%20");
    }

    let url = `${link}54${tel}&text=` + mensaje + numero + " " + msg;

    window.open(url);
  };

  const formatDate = (date) => {
    const createdFormatted = new Date(parseInt(date));

    const createdAt = new Intl.DateTimeFormat("es-AR", {
      dateStyle: "short",
      timeStyle: "short",
    }).format(createdFormatted);

    return createdAt;
  };

  return (
    <React.Fragment>
      <Paper sx={paperCustomStyle}>
        <Box m={1} sx={boxCustomStyle}>
          <Spacer height="8px" />
          <Stack direction="row" spacing={1}>
            <Button
              variant="contained"
              onClick={() => history.push(Urls.serviceUrl(service.nt))}
            >
              Service
            </Button>
            <Button variant="contained" onClick={() => setOpen(true)}>
              Notificar
            </Button>
            <Button variant="contained" onClick={() => window.print()}>
              Imprimir
            </Button>
          </Stack>
          <Spacer height="8px" />
        </Box>
      </Paper>
      <Spacer height="10px" />
      <Paper sx={paperCustomStyle}>
        <Box sx={boxCustomStyle}>
          <TextField
            InputProps={{
              readOnly: true,
            }}
            value={service.name}
            label="Nombre y Apellido"
          />
          <TextField
            InputProps={{
              readOnly: true,
            }}
            value={service.dni}
            label="Documento"
          />
          <TextField
            InputProps={{
              readOnly: true,
            }}
            value={service.phone}
            label="Teléfono"
          />
          <TextField
            InputProps={{
              readOnly: true,
            }}
            value={service.client_email}
            label="Email"
          />
          <TextField
            InputProps={{
              readOnly: true,
            }}
            value={service.receptor}
            label="Recepción"
          />
        </Box>
      </Paper>
      {service.articles.map((service, index) => (
        <div key={index}>
          <Spacer height="10px" />
          <Box sx={paperCustomStyle}>
            <Typography variant="subtitle1">
              Datos del artículo {index + 1}
            </Typography>
          </Box>
          <Paper sx={paperCustomStyle}>
            <Box sx={boxCustomStyle}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={service.item}
                label="Artículo y modelo"
              />
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={service.issue}
                label="Detalle"
              />
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={service.cuantity}
                label="Cantidad"
              />
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={service.serial_number}
                label="Serial"
              />
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={service.accessories}
                label="Accesorios"
              />
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={formatIntlDate(service.bought_at)}
                label="Fecha de Compra"
              />
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                value={service.ticket_number}
                label="Ticket"
              />
            </Box>
            <Spacer height="1px" />
          </Paper>
        </div>
      ))}
      <Spacer height="10px" />
      <Box sx={paperCustomStyle}>
        <Typography variant="subtitle1">Historial de Movimientos</Typography>
      </Box>
      <Paper sx={paperCustomStyle}>
        <Box sx={boxCustomStyle} noValidate autoComplete="off">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Fecha</TableCell>
                  <TableCell>Usuario</TableCell>
                  <TableCell>Estado</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {movementList.map((row, index) => (
                  <TableRow key={index}>
                    <TableCell>{formatDate(row.createdAt)}</TableCell>
                    <TableCell>{row.fixed_by}</TableCell>
                    <TableCell>
                      <Chip
                        label={findStatus(row.status).name}
                        color={findStatus(row.status).color}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Notificar
            </Typography>
            <Typography id="modal-modal-description" sx={{ mt: 2 }}>
              {`Su equipo N° ${service.nt}`}
            </Typography>
            <Spacer height="20px" />
            <FormControl fullWidth sx={{ m: 1 }} variant="standard">
              <TextField
                required
                multiline
                rows={8}
                variant="filled"
                label="Mensaje"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
              />
            </FormControl>
            <Spacer height="20px" />
            <Button
              variant="contained"
              onClick={() => onNotify(service.phone, service.nt)}
            >
              Enviar
            </Button>
            <Spacer height="20px" />
            <Button variant="error" onClick={handleClose}>
              Cerrar
            </Button>
          </Box>
        </Modal>
      </Paper>
    </React.Fragment>
  );
};
