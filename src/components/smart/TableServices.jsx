import * as React from "react";
import { useHistory } from "react-router";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import BuildIcon from "@mui/icons-material/Build";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";

import { Spacer } from "./../dumb/utils";
import {
  notificationMessages,
  paperCustomStyle,
  statusTypes,
  Urls,
} from "./../../utils/constants";

export default function TableServices({
  servicesList,
  getMoreServices,
  getSearchService,
  getSearchServiceDni,
  getSearchServiceStatus,
}) {
  const [statusSearch, setStatusSearch] = React.useState("");
  const [search, setSearch] = React.useState("");
  const [searchDni, setSearchDni] = React.useState("");
  const history = useHistory();

  const keyPress = (e) => {
    if (e.keyCode === 13) {
      getSearchService(parseFloat(e.target.value));
    }
  };

  const keyPressDni = (e) => {
    if (e.keyCode === 13) {
      getSearchServiceDni(e.target.value);
    }
  };

  const handleChangeStatus = (event) => {
    getSearchServiceStatus(event.target.value);
  };

  return (
    <>
      <Box sx={paperCustomStyle}>
        <TextField
          label="Buscar (NT)"
          type="search"
          value={search}
          onKeyDown={keyPress}
          onChange={(e) => setSearch(e.target.value)}
        />
        <TextField
          label="Buscar (DNI)"
          type="search"
          value={searchDni}
          onKeyDown={keyPressDni}
          onChange={(e) => setSearchDni(e.target.value)}
        />
        <Select
          value={statusSearch}
          onChange={(e) => {
            handleChangeStatus(e);
            setStatusSearch(e.target.value);
          }}
        >
          {statusTypes.map((status, index) => (
            <MenuItem key={index} value={status.id}>
              {status.name}
            </MenuItem>
          ))}
        </Select>
      </Box>
      <Spacer />
      <Paper sx={paperCustomStyle}>
        <Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="left">Nota de trabajo</TableCell>
                  <TableCell align="left">Cliente y DNI</TableCell>
                  <TableCell align="right">Artículos</TableCell>
                  <TableCell align="center">Estado</TableCell>
                  <TableCell align="right">Fecha de creación</TableCell>
                  <TableCell align="right">Acción</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {servicesList.map((row, index) => (
                  <TableRow key={index}>
                    <TableCell align="left">{row.name}</TableCell>
                    <TableCell align="left">{row.client}</TableCell>
                    <TableCell align="right">{row.article}</TableCell>
                    <TableCell align="right">{row.status}</TableCell>
                    <TableCell align="right">{row.date}</TableCell>
                    <TableCell align="right">
                      <Stack direction="row" justifyContent="end" spacing={1}>
                        <IconButton
                          color="success"
                          onClick={() => history.push(Urls.detailUrl(row.name))}
                        >
                          <SearchIcon fontSize="small" />
                        </IconButton>
                        <IconButton
                          color="secondary"
                          onClick={() =>
                            history.push(Urls.serviceUrl(row.name))
                          }
                        >
                          <BuildIcon fontSize="small" />
                        </IconButton>
                        <IconButton
                          color="info"
                          onClick={() => {
                            const {
                              phone: tel,
                              nt: numero,
                              status: status_code,
                            } = row.service;

                            let message = notificationMessages[1];

                            switch (status_code) {
                              case 1:
                                message = notificationMessages[1];
                                break;
                              case 2:
                                message = notificationMessages[2];
                                break;
                              case 3:
                                message = notificationMessages[3];
                                break;
                              default:
                                message = notificationMessages[1];
                                break;
                            }

                            let mensaje = "Su equipo N°";
                            let link = "https://api.whatsapp.com/send?phone=";

                            for (let i = 1; i <= mensaje.length; i++) {
                              mensaje = mensaje.replace(" ", "%20");
                            }
                            for (let j = 1; j <= message.length; j++) {
                              message = message.replace(" ", "%20");
                            }

                            let url =
                              `${link}54${tel}&text=` +
                              mensaje +
                              numero +
                              message;

                            window.open(url);
                          }}
                        >
                          <WhatsAppIcon fontSize="small" />
                        </IconButton>
                      </Stack>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Paper>
      <Spacer />
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Button variant="contained" onClick={getMoreServices}>
          Cargar más
        </Button>
      </div>
    </>
  );
}
