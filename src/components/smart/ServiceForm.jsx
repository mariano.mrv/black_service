import React from "react";
import { ref, uploadBytes, getDownloadURL, listAll } from "firebase/storage";

import Stack from "@mui/material/Stack";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import IconButton from "@mui/material/IconButton";
import LaunchIcon from "@mui/icons-material/Launch";

import { Spacer } from "./../dumb/utils";
import {
  statusTypes,
  paperCustomStyle,
  boxCustomStyle,
  formatIntlDate,
} from "./../../utils/constants";

export const ServiceForm = ({
  service,
  storage,
  handleChangeStatus,
  handleUpdateService,
  handleConfirmService,
}) => {
  const [imageUrls, setImageUrls] = React.useState([]);

  const onAddFile = async (event) => {
    const image = event.target.files[0];
    if (!image) return;

    const imageName = `images/${service.nt}/${image.name}`;
    const imageRef = await ref(storage, imageName);

    await uploadBytes(imageRef, image).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        setImageUrls((prev) => [...prev, url]);
      });
    });
  };

  React.useEffect(() => {
    listAll(ref(storage, `images/${service.nt}/`)).then((response) => {
      response.items.forEach((item) => {
        getDownloadURL(item).then((url) => {
          setImageUrls((prev) => [...prev, url]);
        });
      });
    });
  }, [service.nt]);

  return (
    <React.Fragment>
      <Box sx={paperCustomStyle}>
        <Typography variant="subtitle1">Cliente</Typography>
      </Box>
      <Paper sx={paperCustomStyle}>
        <Box sx={boxCustomStyle}>
          <TextField disabled value={service.name} label="Nombre y Apellido" />
          <TextField disabled value={service.dni} label="Documento" />
          <TextField disabled value={service.phone} label="Teléfono" />
          <TextField disabled value={service.client_email} label="Email" />
          <TextField
            disabled
            value={service.bought_at}
            label="Fecha de Compra"
          />
          <TextField disabled value={service.receptor} label="Recepción" />
        </Box>
      </Paper>
      <Spacer height="10px" />
      <Box sx={paperCustomStyle}>
        <Typography variant="subtitle1">Informe Técnico</Typography>
      </Box>
      <Paper sx={paperCustomStyle}>
        <Box sx={boxCustomStyle}>
          <TextField
            required
            label="Informe Técnico"
            value={service.report}
            onChange={(e) => handleUpdateService("report", e.target.value)}
          />
          <TextField
            required
            label="Precio"
            value={service.fix_price}
            onChange={(e) => handleUpdateService("fix_price", e.target.value)}
          />
          <TextField
            required
            label="Técnico"
            helperText="Reparado por"
            value={service.fixed_by}
            onChange={(e) => handleUpdateService("fixed_by", e.target.value)}
          />
          <Box m={1} sx={boxCustomStyle}>
            <Stack direction="row" spacing={1}>
              <Select value={service.status} onChange={handleChangeStatus}>
                {statusTypes.map((status, index) => (
                  <MenuItem key={index} value={status.id}>
                    {status.name}
                  </MenuItem>
                ))}
              </Select>
            </Stack>
            <Spacer height="8px" />
          </Box>
        </Box>
      </Paper>
      <Spacer height="10px" />
      {service.articles.map((service, index) => (
        <div key={index}>
          <Spacer height="10px" />
          <Box sx={paperCustomStyle}>
            <Typography variant="subtitle1">
              Datos del artículo {index + 1}
            </Typography>
          </Box>
          <Paper sx={paperCustomStyle}>
            <Box sx={boxCustomStyle}>
              <TextField
                disabled
                value={service.item}
                label="Artículo y modelo"
              />
              <TextField disabled value={service.issue} label="Detalle" />
              <TextField disabled value={service.cuantity} label="Cantidad" />
              <TextField
                disabled
                value={service.serial_number}
                label="Serial o Modelo"
              />
              <TextField
                disabled
                value={service.accessories}
                label="Accesorios"
              />
              <TextField
                disabled
                value={formatIntlDate(service.bought_at)}
                label="Fecha de Compra"
              />
              <TextField
                disabled
                value={service.ticket_number}
                label="Ticket"
              />
            </Box>
            <Spacer height="1px" />
          </Paper>
        </div>
      ))}
      <Spacer height="10px" />
      <Paper sx={paperCustomStyle}>
        <Box m={1} sx={boxCustomStyle}>
          <Spacer height="8px" />
          <Stack direction="row" spacing={1}>
            <Button variant="contained" onClick={handleConfirmService}>
              Guardar
            </Button>
          </Stack>
          <Spacer height="8px" />
        </Box>
      </Paper>
      <Box sx={paperCustomStyle}>
        <Typography variant="subtitle1">Archivos (Menos de 1MB)</Typography>
      </Box>
      <Paper sx={paperCustomStyle}>
        <Box m={1} sx={boxCustomStyle}>
          <Spacer height="8px" />
          <Stack direction="row" spacing={1}>
            <Button variant="contained" component="label">
              Subir
              <input type="file" hidden onChange={onAddFile} />
            </Button>
          </Stack>
          <Spacer height="8px" />
          <ImageList
            sx={{ width: "100%", height: 450 }}
            cols={3}
            rowHeight={164}
          >
            {imageUrls.map((url, index) => (
              <ImageListItem key={index}>
                <img
                  src={`${url}?w=164&h=164&fit=crop&auto=format`}
                  srcSet={`${url}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                  alt={`image_${index}`}
                  loading="lazy"
                />
                <ImageListItemBar
                  actionIcon={
                    <>
                      <IconButton
                        sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                        onClick={() => window.open(url)}
                      >
                        <LaunchIcon />
                      </IconButton>
                    </>
                  }
                />
              </ImageListItem>
            ))}
          </ImageList>
        </Box>
      </Paper>
    </React.Fragment>
  );
};
