import * as React from "react";

import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import Typography from "@mui/material/Typography";
import { Stack } from "@mui/material";

import { paperCustomStyle, boxCustomStyle } from "../../utils/constants";
import { Spacer } from "./../dumb/utils";
import { ServiceArticlesBody, ServiceBody } from "../../utils/models";

export default function CreateServiceForm({ isLoading, onCreateService }) {
  const [form, setForm] = React.useState(ServiceBody);
  const [articlesForm, setArticlesForm] = React.useState([ServiceArticlesBody]);

  const handleCreateService = () => {
    if (!form.name || !form.phone || !form.receptor)
      return alert("se deben completar los campos obligatorios");

    const newService = {
      ...form,
      articles: articlesForm,
    };

    onCreateService(newService);
  };

  return (
    <React.Fragment>
      <Box sx={paperCustomStyle}>
        <Typography variant="subtitle1">Datos del cliente</Typography>
      </Box>
      <Spacer height="1px" />
      <Paper sx={paperCustomStyle}>
        <Box sx={boxCustomStyle}>
          <TextField
            required
            label="Nombre y Apellido"
            value={form.name}
            onChange={(e) => setForm({ ...form, name: e.target.value })}
          />
          <TextField
            required
            label="Teléfono"
            helperText="Fijo o móvil"
            value={form.phone}
            onChange={(e) => setForm({ ...form, phone: e.target.value })}
          />
          <TextField
            label="Email"
            value={form.client_email}
            onChange={(e) => setForm({ ...form, client_email: e.target.value })}
          />
          <TextField
            label="Documento"
            value={form.dni}
            onChange={(e) => setForm({ ...form, dni: e.target.value })}
          />
        </Box>
        <Box sx={boxCustomStyle}>
          <Checkbox
            color="primary"
            checked={form.report_price}
            onChange={(e) =>
              setForm({ ...form, report_price: e.target.checked })
            }
          />
          Informar precio antes de reparar
        </Box>
      </Paper>
      {articlesForm.map((article, index) => {
        const handleAddArticle = () => {
          if (!article.item || !article.issue)
            return alert(
              "se deben completar los campos obligatorios del articulo"
            );

          setArticlesForm([
            ...articlesForm,
            {
              ...ServiceArticlesBody,
              item_number: index + 1,
            },
          ]);
        };

        const allowNewArticle =
          articlesForm.length === index + 1 && articlesForm.length !== 10;

        const filteredArticleList = articlesForm.filter(
          (article) => article.item_number !== index
        );

        const currentArticle = articlesForm.find(
          (article) => article.item_number === index
        );

        return (
          <React.Fragment key={index}>
            <Spacer height="10px" />
            <Box sx={paperCustomStyle}>
              <Typography variant="subtitle1">Datos del artículo</Typography>
            </Box>
            <Paper sx={paperCustomStyle}>
              <Box sx={boxCustomStyle}>
                <TextField
                  required
                  label="Artículo"
                  type="search"
                  helperText="Artículo, modelo, color... etc"
                  value={article.item}
                  onChange={(e) => {
                    setArticlesForm([
                      ...filteredArticleList,
                      { ...currentArticle, item: e.target.value },
                    ]);
                  }}
                />
                <TextField
                  required
                  label="Detalle"
                  helperText="Descripción del problema"
                  value={article.issue}
                  onChange={(e) => {
                    setArticlesForm([
                      ...filteredArticleList,
                      { ...currentArticle, issue: e.target.value },
                    ]);
                  }}
                />
                <TextField
                  label="Cantidad"
                  helperText="Cantidad de artículos"
                  value={article.cuantity}
                  onChange={(e) => {
                    setArticlesForm([
                      ...filteredArticleList,
                      { ...currentArticle, cuantity: e.target.value },
                    ]);
                  }}
                />
                {/* <Box m={1} sx={boxCustomStyle}>
                  <Stack direction="row" spacing={1}>
                    <Button variant="contained" component="label">
                      Imagen del Artículo
                      <input type="file" hidden />
                    </Button>
                  </Stack>
                  <Spacer height="8px" />
                </Box> */}
                <TextField
                  label="Modelo"
                  helperText="N° de serial o modelo"
                  value={article.serial_number}
                  onChange={(e) => {
                    setArticlesForm([
                      ...filteredArticleList,
                      { ...currentArticle, serial_number: e.target.value },
                    ]);
                  }}
                />
                <TextField
                  label="Accesorios"
                  helperText="Cargador, memoria, bateria, etc..."
                  value={article.accessories}
                  onChange={(e) => {
                    setArticlesForm([
                      ...filteredArticleList,
                      { ...currentArticle, accessories: e.target.value },
                    ]);
                  }}
                />
                <TextField
                  helperText="Fecha de Compra"
                  type="date"
                  required
                  onChange={(e) => {
                    try {
                      const date = new Date(e.target.value)
                        .valueOf()
                        .toString();

                      setArticlesForm([
                        ...filteredArticleList,
                        { ...currentArticle, bought_at: date },
                      ]);
                    } catch (error) {
                      setArticlesForm([
                        ...filteredArticleList,
                        { ...currentArticle, bought_at: "" },
                      ]);
                    }
                  }}
                />
                <TextField
                  label="Numero de ticket"
                  helperText="Ticket de compra"
                  type="search"
                  value={article.ticket_number}
                  onChange={(e) => {
                    setArticlesForm([
                      ...filteredArticleList,
                      { ...currentArticle, ticket_number: e.target.value },
                    ]);
                  }}
                />
              </Box>
              {allowNewArticle && (
                <>
                  <Box m={1}>
                    <Button variant="contained" onClick={handleAddArticle}>
                      + Agregar más
                    </Button>
                  </Box>
                  <Spacer height="1px" />
                </>
              )}
            </Paper>
            <Spacer height="10px" />
          </React.Fragment>
        );
      })}
      <Box sx={paperCustomStyle}>
        <Typography variant="subtitle1">Receptor</Typography>
      </Box>
      <Paper sx={paperCustomStyle}>
        <Box sx={boxCustomStyle}>
          <TextField
            label="Recepción"
            helperText="ID Receptor"
            type="search"
            required
            value={form.receptor}
            onChange={(e) => setForm({ ...form, receptor: e.target.value })}
          />
        </Box>
      </Paper>
      <Spacer />
      <Paper sx={paperCustomStyle}>
        <Box m={1} sx={boxCustomStyle}>
          <Spacer height="8px" />
          <Button
            disabled={isLoading}
            variant="contained"
            onClick={handleCreateService}
          >
            Crear
          </Button>
          <Spacer height="8px" />
        </Box>
      </Paper>
    </React.Fragment>
  );
}
