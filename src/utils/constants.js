export const statusTypes = [
  { id: 1, name: "Pendiente", color: "warning" },
  { id: 2, name: "Reparado", color: "success" },
  { id: 3, name: "Problema", color: "error" },
  { id: 4, name: "Entregado", color: "info" },
  { id: 5, name: "Caducado", color: "secondary" },
];

export const findStatus = (statusId) =>
  statusTypes.find((status) => status.id === statusId);

const padTo2Digits = (num) => {
  return num.toString().padStart(2, "0");
};

export const formatDate = (date) => {
  return (
    [
      date.getFullYear(),
      padTo2Digits(date.getMonth() + 1),
      padTo2Digits(date.getDate()),
    ].join("-") +
    " " +
    [
      padTo2Digits(date.getHours()),
      padTo2Digits(date.getMinutes()),
      padTo2Digits(date.getSeconds()),
    ].join(":")
  );
};

export const formatIntlDate = (date) => {
  if (!date) return "";

  return new Intl.DateTimeFormat("es-AR", {
    dateStyle: "short",
    timeStyle: "short",
  }).format(date);
};

export const Urls = {
  homeUrl: () => `/`,
  detailUrl: (id) => `/detail/${id}`,
  serviceUrl: (id) => `/service/${id}`,
};

export const paperCustomStyle = { maxWidth: 936, margin: "auto" };
export const boxCustomStyle = {
  "& .MuiTextField-root": { m: 1, width: "25ch" },
};

export const notificationMessages = {
  1: " esta siendo procesado por nuestro CENTRO DE SERVICIO. El horario de atención es de Lunes a Sabado de 9:30 a 21:30. Gracias -Mensaje enviado automaticamente, no responder",
  2: " esta listo para ser retirado de nuestro CENTRO DE SERVICIO. El horario de atención es de Lunes a Sabado de 9:30 a 21:30. Gracias -Mensaje enviado automaticamente, no responder",
  3: " presenta problemas, por favor presentarse en el CENTRO DE SERVICIO. El horario de atención es de Lunes a Sabado de 9:30 a 21:30. Gracias -Mensaje enviado automaticamente, no responder",
};
