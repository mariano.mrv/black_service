export const ServiceBody = {
  createdAt: "",
  nt: "",
  name: "",
  phone: "",
  client_email: "",
  dni: "",
  fixed_by: "",
  fix_price: "",
  report: "",
  report_price: false,
  receptor: "",
  status: 1,
};

export const ServiceArticlesBody = {
  item_number: 0,
  item: "",
  issue: "",
  cuantity: 1,
  image_url: "",
  serial_number: "",
  accessories: "",
  bought_at: "",
  ticket_number: "",
  point_sale: 1,
};
